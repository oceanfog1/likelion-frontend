import BoardList from "../components/board/BoardList";
import Board from "../components/board/Board";
import {useSearchParams} from "react-router-dom";

function BoardPage() {
    const [searchParams] = useSearchParams();

    const id = searchParams.get('id');

    return (
        <>
            <h1>게시판</h1>
            <hr/>
            {id ? <Board id={id}/> : <BoardList/>}
        </>
    );
}

export default BoardPage;