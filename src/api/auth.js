import axiosPost from "./axiosPost";
import {API_BASE_URL, ENDPOINT} from "../constant";

export async function login(userName, password) {
    const url = API_BASE_URL + ENDPOINT.LOGIN;
    const data = {userName, password};
    return await axiosPost({url, data})
}

export async function join(userName, password) {
    const url = API_BASE_URL + ENDPOINT.JOIN;
    const data = {userName, password};
    return await axiosPost({url, data});
}