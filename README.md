# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## 사용방법

root에 .env 파일을 생성 합니다.

아래 localhost로 되어 있는 주소를 실제 주소로 바꾸면 됩니다.

.env는 git에 올리지 않습니다.

```env
REACT_APP_API_BASE_URL="http://localhost:8080
```


