import {Button, Form} from "react-bootstrap";
import React, {useEffect, useState} from "react";
import {useRecoilState} from "recoil";
import {tokenState} from "../../recoil";
import {writePost} from "../../api/posts";

function BoardNew() {
    const [title, setTitle] = useState('');
    const [body, setBody] = useState('');
    const [token, setToken] = useRecoilState(tokenState);

    useEffect(() => {
        if (token === '') window.location.href="/boards";
    }, [])

    return (
        <>
            <h1>게시물 등록</h1>
            <hr/>
            <Form>
                <Form.Group className="mb-3" controlId="formTitle">
                    <Form.Control type="text" placeholder="제목" onChange={(e) => {setTitle(e.target.value)}}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBody">
                    <Form.Control as="textarea" rows={5} placeholder="본문" onChange={(e) => {setBody(e.target.value)}}/>
                </Form.Group>
                <Button variant="primary" onClick={() =>
                    writePost(title, body, token).then((res) => {
                        alert(res.data.result.message);
                        window.location.href = `/boards?id=${res.data.result.postId}`;
                    }).catch((err) => alert(err.data.result.message))
                }>
                    작성
                </Button>
            </Form>
        </>
    );
}

export default BoardNew;