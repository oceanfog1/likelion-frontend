import './App.css';
import NavigationBar from "./components/NavigationBar";
import {Container} from "react-bootstrap";
import Pages from "./pages/Pages";
import Footer from "./components/Footer";
import {RecoilRoot} from "recoil";
function App() {
    return (
        <>
            <RecoilRoot>
                <NavigationBar/>
                <Container className="content-wrapper my-5">
                    <Pages/>
                </Container>
                <Footer/>
            </RecoilRoot>
        </>
    );
}

export default App;
