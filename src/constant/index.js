export const API_BASE_URL = process.env.REACT_APP_API_BASE_URL;
export const ENDPOINT = {
    LOGIN: "/api/v1/users/login",
    JOIN: "/api/v1/users/join",
    POST: "/api/v1/posts"
};