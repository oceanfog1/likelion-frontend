import {Button, Container, Nav, Navbar} from "react-bootstrap";
import {useRecoilState} from "recoil";
import {tokenState} from "../recoil";

function NavigationBar() {
    const [token, setToken] = useRecoilState(tokenState);

    return(
        <Navbar bg="light" expand="lg">
            <Container>
                <Navbar.Brand href="/"><strong>MutsaSNS</strong></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link href="/boards">게시판</Nav.Link>
                    </Nav>
                    <Nav>
                        {/*로그인 여부 확인 후 로그인 버튼 또는 내정보 버튼, 로그아웃 버튼*/
                            token === '' ?
                                <>
                                    <Button className="float-end me-2" variant="primary" href="/login">로그인</Button>
                                    <Button className="float-end me-2" variant="primary" href="/join">회원가입</Button>
                                </>
                                : <Button className="float-end" variant="primary" onClick={() => {
                                    setToken('');
                                    window.location.href = "/";
                                }}>로그아웃</Button>
                        }
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default NavigationBar;