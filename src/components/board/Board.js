import {useEffect, useState} from "react";
import {Button, Card} from "react-bootstrap";
import {getPost} from "../../api/posts";

function Board(props) {
    const [board, setBoard] = useState({});

    useEffect(() => {
        getPost(props.id).then((res) => setBoard(res.data))
            .catch(() => window.location.href="/boards")
    }, []);

    return (
        <>
            <Card>
                <Card.Header as="h3">제목 : {board.title}</Card.Header>
                <Card.Body>
                    <Card.Title>작성자 : {board.userName}</Card.Title>
                    <Card.Text>
                        {board.body}
                    </Card.Text>
                </Card.Body>
            </Card>
        </>
    );
}

export default Board