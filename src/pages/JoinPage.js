import {Button, Form} from "react-bootstrap";
import {join} from "../api/auth";
import React, {useState} from "react";

function JoinPage() {
    const [username, setUsername] = useState();
    const [password, setPassword] = useState();
    const [checkPassword, setCheckPassword] = useState();

    return (
        <>
            <h1>Join</h1>
            <hr/>
            <Form>
                <Form.Group className="mb-3" controlId="formUsername">
                    <Form.Label>Username</Form.Label>
                    <Form.Control type="text" placeholder="Enter username" onChange={(e) => {setUsername(e.target.value)}}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" onChange={(e) => {setPassword(e.target.value)}}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formCheckPassword">
                    <Form.Label>Password Check</Form.Label>
                    <Form.Control type="password" placeholder="Password" onChange={(e) => {setCheckPassword(e.target.value)}}/>
                </Form.Group>
                <Button variant="primary" onClick={() => {
                    if (password !== checkPassword) {
                        alert("비밀번호를 확인해주세요.");
                        return;
                    }
                    join(username, password)
                        .then((res) => {
                            alert("회원가입 완료!");
                            window.location.href = "/";
                        })
                        .catch((err) => alert(err.response.data.result.message))
                }}>
                    Join
                </Button>
            </Form>
        </>
    );
}

export default JoinPage;